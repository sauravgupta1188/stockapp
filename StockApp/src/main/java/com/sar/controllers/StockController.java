package com.sar.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sar.bean.ErrorResponse;
import com.sar.bean.Stock;
import com.sar.exception.StockException;

@RestController

public class StockController {

	@RequestMapping(value = "/getStockPrice", method = RequestMethod.GET)
	public ResponseEntity<Stock> getStockPrice1(
			@RequestParam(value = "name") final String name,
			@RequestParam(value = "currency", required = false, defaultValue = "USD") final String currency) throws StockException {

		Stock stock = new Stock();
		stock.setName(name);
		stock.setCurrency(currency);
		stock.setPrice(Math.random()*100);
		

		if ( name.length()<4) {
			throw new StockException("Invalid Stock requested");
		}

		return new ResponseEntity<Stock>(stock, HttpStatus.OK);
	}

	@ExceptionHandler(StockException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
}