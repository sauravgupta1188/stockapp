package com.sar.exception;

public class StockException extends Exception {
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}
	public StockException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public StockException() {
		super();
	}
}