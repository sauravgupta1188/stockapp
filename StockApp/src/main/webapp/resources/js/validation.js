function getStock(){
	
	$('#result').empty();
	
    if($('#sname').val().length>=4){
    	
	  $.ajax({
			url: "getStockPrice?name="+$('#sname').val()+"&currency="+$('#currency').val(),
	   		type: 'GET',
			beforeSend : function(){
				ajaxindicatorstart('Fetching Results.. please wait..');
			},
			success : function(jsonData){
				
				if(jsonData["price"]!=undefined){
					$('#result').append(' Current Stock Price is '+jsonData["price"].toFixed(2)+ " "+jsonData["currency"]);
				}else 	if(jsonData["message"]!=undefined){
					$('#result').append(jsonData["message"]);
				}
			},
			complete : function(){
				ajaxindicatorstop();
				
			},
			error : function(data){
				ajaxindicatorstop();
			}
		} );
	}else{
		$('#result').append('<font color="red">Stock name should be minimum of 4 characters</font>');
	}

}

 
